package com.example.app;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.opencsv.CSVReader;



public class App 
{
	public static void main( String[] args )
	{
		System.out.println( "Hello World!" );

		String filename= "MOCK_DATA__269__0.csv";

		FileReader fileReader=null;
		CSVReader Reader = null;
		try {
			fileReader = new FileReader(filename);

			char separator = ',';
			Reader = new CSVReader (fileReader, separator);

			List <User> userList = new ArrayList <>();

			String[] line = null;
			while((line = Reader.readNext()) != null) {
				long  id = Long.valueOf(line[0]);
				String firstname = line[1];
				String lastname = line[2];
				String email = line[3];
				String phone = line[4];

				User newUser = new User(id,firstname,lastname,email,phone);
				userList.add(newUser);
				System.out.println("Utilisateur "+ id + " ajouté");				

			}

			App.introduceUsers(userList);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.err.println("Le fichier" + filename + " est introuvable");
		}
		catch (IOException e) {
			e.printStackTrace();
			System.err.println("uneerreur est survenu sur lors de la lecture");
		}



	}

	private static void introduceUsers(List<User> userList) {
		for (Iterator iterator = userList.iterator(); iterator.hasNext();) {
			User user = (User) iterator.next();
			user.introduceMe();
			
		}// TODO Auto-generated method stub
		
	}
}
