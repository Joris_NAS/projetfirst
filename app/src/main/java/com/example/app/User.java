package com.example.app;

public class User {
	
	long id;
	String firstname;
	String lastname;
	String email;
	String phone;
	
	public User(long id,String firstname,String lastname, String email,String phone) {
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.phone = phone;
		
	}

	public void introduceMe () {
		System.out.println("hey je suis " + firstname );
	}
}
